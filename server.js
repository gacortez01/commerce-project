const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const path = require('path');

app.use(bodyParser.json());
//app.use(express.static(path.join(__dirname, '/static'))); 
app.use(express.static(path.join(__dirname, 'commerce-app', 'dist')));

//-----------------------------------------------------------------
mongoose.connect('mongodb://localhost/test1proj');

var ProductSchema = new mongoose.Schema({
  name: {type: String, required: [true, 'Name is required'], minlength: [3, 'Name must be at least 3 characters long']},
  price: {type: Number, required: true, min: [.01, 'Price must be at least $0.01']},
  quantity: {type: Number, required: true, min: [1, 'Quantity must be at least 1']}
}, {timestamps:true})


mongoose.model('Product', ProductSchema);
var Product = mongoose.model('Product', ProductSchema);

app.post('/api/products', (req, res)=>{
  var newProduct = new Product(req.body);
  newProduct.save((err)=>{
      if(err){res.json(err)}
      else{res.json(newProduct)}
  })
})

app.get('/api/products', (req, res)=>{
  Product.find({}, (err, foundProducts)=>{
      if(err){console.log(err)}
      else{res.json(foundProducts)}
  })
})

app.get('/api/products/:id', (req, res)=>{
  Product.findOne({_id: req.params.id}, (err, foundProduct)=>{
      if(err){console.log(err)}
      else{res.json(foundProduct)}       
  })
})

app.put('/api/products/:id', (req, res)=>{
  Product.findOne({_id: req.params.id}, (err, foundProduct)=>{
    if (err) {console.log(err)}
    else{
      foundProduct.name = req.body.name;
      foundProduct.quantity = req.body.quantity;
      foundProduct.price = req.body.price;
      foundProduct.save((err)=>{
        if(err){res.json(err)}
        else{res.json(foundProduct)}
      })
    }
  })
});

app.delete('/api/products/:id', (req, res)=>{
  Product.remove({_id: req.params.id}, (err)=>{
    if(err){console.log(err)}
    else{res.json({succes: "Product destroyed"})}
  })
})

//------------------------------------------------------------------
app.all("*", (req,res,next) => {
  res.sendFile(path.resolve("./commerce-app/dist/index.html"))
});

app.listen(6789, ()=>{
  console.log('listening on port 6789');
})
